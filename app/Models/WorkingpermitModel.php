<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkingpermitModel extends Model
{
    // $name = DB::table('working_permit')->where('name', 'John')->pluck('name');
    protected $table = 'working_permit';
    protected $fillable = [
        'nama_pemohon', 'nama_perusahaan', 'jabatan_pemohon', 'kontak_perusahaan'
    ];
    
}
