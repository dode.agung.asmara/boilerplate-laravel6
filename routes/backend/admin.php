<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\WorkingpermitController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/workingpermit', [WorkingpermitController::class, 'index'])->name('workingpermit');
Route::get('/workingpermit/show/{params}', [WorkingpermitController::class, 'show'])->name('/workingpermit/show/{params}');
Route::get('/workingpermit/showall', [WorkingpermitController::class, 'all'])->name('/workingpermit/showall');
